package de.immonet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by to be announced
 */

public class CookieHunter {

	static {
		Map<String, String> morseCode = new HashMap<>();
		morseCode.put("A", ".-");
		morseCode.put("B", "-...");
		morseCode.put("C", "-.-.");
		morseCode.put("D", "-..");
		morseCode.put("E", ".");
		morseCode.put("F", "..-.");
		morseCode.put("G", "--.");
		morseCode.put("H", "....");
		morseCode.put("I", "..");
		morseCode.put("J", ".---");
		morseCode.put("K", "-.-");
		morseCode.put("L", ".-..");
		morseCode.put("M", "--");
		morseCode.put("N", "-.");
		morseCode.put("O", "---");
		morseCode.put("P", ".--.");
		morseCode.put("Q", "--.-");
		morseCode.put("R", ".-.");
		morseCode.put("S", "...");
		morseCode.put("T", "-");
		morseCode.put("U", "..-");
		morseCode.put("V", "...-");
		morseCode.put("W", ".--");
		morseCode.put("X", "-..-");
		morseCode.put("Y", "-.--");
		morseCode.put("Z", "--..");
	}

	public void hunt(HttpServletRequest request, HttpServletResponse response) {

		//TODO use this
	}
}
