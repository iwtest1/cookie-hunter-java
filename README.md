# CookieHunter

## Mission: Write a tested method that analyzes a HttpServletRequest and handles a cookie!

* Cookie value contains a morse code and has to be translated into a readable string.
* Cookie name is "optimusPrime".
* If the translated value is "DELETE", delete the cookie.


## Libs

The following libs are provided:
* javax servlet api
* junit
* mockito
* hamcrest matchers


## Ready to go?

Before starting, create a feature branch with your name using git command line or the IDE.


## Finished?

Commit your changes with a comment of your choice.
